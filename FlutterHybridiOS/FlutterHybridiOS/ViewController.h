//
//  ViewController.h
//  FlutterHybridiOS
//
//  Created by 冯俊铭 on 2020/3/22.
//  Copyright © 2020 cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)onSwitch:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *switchLable;
@property (weak, nonatomic) IBOutlet UILabel *showLabel;
- (IBAction)onBack:(id)sender;
- (IBAction)editChange:(id)sender;


@end
