//
//  NSObject+FirstViewController.h
//  FlutterHybridiOS
//
//  Created by 冯俊铭 on 2020/3/24.
//  Copyright © 2020 cody. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FirstViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *inputParams;

@end

NS_ASSUME_NONNULL_END

