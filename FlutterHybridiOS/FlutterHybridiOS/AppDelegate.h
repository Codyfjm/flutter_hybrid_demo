//
//  AppDelegate.h
//  FlutterHybridiOS
//
//  Created by 冯俊铭 on 2020/3/22.
//  Copyright © 2020 cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
